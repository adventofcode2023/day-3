use std::collections::HashMap;

use gear_ratios::get_lines;
use gear_ratios::parsing::has_adjacent_symbols;
use gear_ratios::parsing::parse_line;
use gear_ratios::parsing::Coordinates;
use gear_ratios::FileToParse::Input;

fn process(lines: impl Iterator<Item = String>) -> u32 {
    let mut numbers: HashMap<(Coordinates, Coordinates), u32> = HashMap::new();
    let mut symbols: HashMap<Coordinates, char> = HashMap::new();

    lines.enumerate().for_each(|(line_number, line)| {
        parse_line(&mut numbers, &mut symbols, &line, line_number);
    });

    numbers
        .iter()
        .filter(|((start, end), _)| has_adjacent_symbols(&mut symbols, start, end))
        .map(|(_, val)| *val)
        .sum()
}

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;
    // let lines = get_lines(Test(1))?;

    let result = process(lines);

    println!("{result}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use gear_ratios::FileToParse::Test;

    #[test]
    fn test_process() {
        let fixture = get_lines(Test(1)).expect("Failed to load test-1.txt");
        assert_eq!(4361, process(fixture));
    }

    #[test]
    fn test_head() {
        let fixture = r#"
.....
@123.
.....
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }

    #[test]
    fn test_tail() {
        let fixture = r#"
.....
.123@
.....
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }

    #[test]
    fn test_top_leading_diagonal() {
        let fixture = r#"
@....
.123.
.....
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }
    #[test]
    fn test_top_trailing_diagonal() {
        let fixture = r#"
....@
.123.
.....
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }

    #[test]
    fn test_bottom_leading_diagonal() {
        let fixture = r#"
.....
.123.
@....
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }
    #[test]
    fn test_bottom_trailing_diagonal() {
        let fixture = r#"
.....
.123.
....@
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }

    #[test]
    fn test_top() {
        let fixture = r#"
.@...
.123.
.....
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }

    #[test]
    fn test_bottom() {
        let fixture = r#"
.....
.123.
..@..
"#;
        assert_eq!(123, process(fixture.lines().map(|s| s.to_owned())));
    }
}
