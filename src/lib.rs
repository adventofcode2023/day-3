use std::fs::File;
use std::io::{self, prelude::*};
use std::path::PathBuf;

use FileToParse::*;

pub mod parsing;

#[derive(Debug, Default)]
pub enum FileToParse {
    #[default]
    Input,
    Test(u32),
}

impl ToString for FileToParse {
    fn to_string(&self) -> String {
        match self {
            Input => String::from("input.txt"),
            Test(n) => format!("test-{n}.txt"),
        }
    }
}

impl FileToParse {
    pub fn path(&self) -> PathBuf {
        PathBuf::from(self.to_string())
    }
}

pub fn get(target: FileToParse) -> Result<String, std::io::Error> {
    let mut file = File::open(target.path())?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    Ok(input)
}

pub fn get_lines(target: FileToParse) -> Result<impl Iterator<Item = String>, std::io::Error> {
    let file = File::open(target.path())?;

    Ok(io::BufReader::new(file).lines().map(Result::unwrap))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn filename_from_enum() {
        println!("{}", Input.to_string());
        println!("{}", Test(1).to_string());
    }
}
