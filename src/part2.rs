use gear_ratios::{parsing::is_adjacent_to, FileToParse::Input};
use std::collections::HashMap;

use gear_ratios::{
    get_lines,
    parsing::{parse_line, Coordinates},
};

fn process(lines: impl Iterator<Item = String>) -> u32 {
    let mut numbers: HashMap<(Coordinates, Coordinates), u32> = HashMap::new();
    let mut symbols: HashMap<Coordinates, char> = HashMap::new();

    lines.enumerate().for_each(|(line_number, line)| {
        parse_line(&mut numbers, &mut symbols, &line, line_number);
    });

    let gears: Vec<_> = symbols
        .iter()
        .filter(|(_, c)| **c == '*')
        .filter(|(pos, _)| {
            numbers
                .iter()
                .filter(|((start, end), _)| is_adjacent_to(start, end, **pos))
                .count()
                == 2
        })
        .collect();

    gears
        .iter()
        .map(|(pos, _)| {
            numbers
                .iter()
                .filter_map(|((start, end), value)| {
                    if is_adjacent_to(start, end, **pos) {
                        Some(value)
                    } else {
                        None
                    }
                })
                .product::<u32>()
        })
        .sum()
}

fn main() -> std::io::Result<()> {
    let lines = get_lines(Input)?;

    let result = process(lines);

    println!("{result}");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use gear_ratios::get_lines;
    use gear_ratios::FileToParse::Test;

    #[test]
    fn test() {
        let fixture = get_lines(Test(1)).expect("Failed to load test-1.txt");
        assert_eq!(467835, process(fixture));
    }
}
